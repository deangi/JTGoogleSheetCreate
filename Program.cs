﻿using ProTechnologyAutomation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using JetTracByodGoogleApiLibrary;
using jettrac_byod_google_api_library;

//=====================================================================
// JTGoogleSheetCreate - (c) 2021, Pro Technology Automation, Inc.
//
// V1.00 3/7/2021 Dean Gienger
// V1.2 5/2/2021 - Dean - add folder
// V1.3 6/22/2021 - Dean - Additions
//   1) Add price extended formula = cost/item * quantity
//     - Change to INI file  Formula=F=C#*D#  - column F in every row = Cr*Dr, replace # with row number,
//       place formula in column F (done)
//   2) For every row that contains a specified search string like "------" highlight in yellow (done)
//   3) In row 1 to the cell located to the right of the right most label we want to have a formula that would
//      sum the Extended Cost of all rows below. (done)
//   4) Can you freeze the first row?  Manually, I did it by selecting the first row then clicking View,
//      Freeze, 1 row (done)
//   5) Can you make the cells in the first row bold and underlined to stand out? (todo)
//   6) For this idea, I don't know if it is possible.  But it would be great to somehow click a
//      "button" (or however it could be done) to invoke a sort to bring all non-zero quantities to the top.
//      Manually, I did it by selecting all cells then clicking Data, Sort Range, selecting "Data has header row",
//      then "Sort by Quantity A->Z (todo)
//=====================================================================
namespace JetTracJTGoogleSheetCreate
{
    class Program
    {

        /// <summary>
        /// Constructor
        /// </summary>
        private Program() { }

        public static readonly string sVersion = " V1.3 (6/22/2021) ";
        public static readonly string sProgName = "JTGoogleSheetCreate";
        public static readonly string sProgDesc = "JTGoogleSheetCreate Description";
        public static readonly string sCopyright = "(c) 2021, Pro Technology Automation Incorporated, Simi Valley, CA";


        /// <summary>
        /// Write a sign-on message to the console
        /// </summary>
        private void SignOn()
        {
            Console.WriteLine(sProgName + " " + sVersion + " - " + sProgDesc);
            Console.WriteLine(sCopyright + "\n\n");
        }


        /// <summary>
        /// Stores configuration data from the INI or config file
        /// </summary>
        class ConfigData
        {
            // command line parameters
            public string inputFileName;
            public string outputFileName;
            public string iniFileName;
            public string jfcLogFileName;

            // initialization file parameters
            // in the input xml we find a row/column spread-sheet type of data structure
            // <table>
            //   <row>
            //     <c1data>1</c1data>
            //     <c2data>2</c2data>
            //   </row>
            // </table>
            public string xmlTableName; // name of outer element that contains the rows elements TableName=rows
            public string xmlRowName; // name of inner element that contains each row  RowName=row
            public int    formulaColumn; // Column to create formula, F=C#*DE  from INI, "F" here means column[5]
            public string formulaTemplate; // "=C#*D#" - V1.3
            public string highlightSearchString; // string to search for to highlight a row in yellow - V1.3

            // google drive folder for creating the sheet
            public string gdriveFolder; // GdriveFolder=timesheets/weekly
            // name of tag to use to extract the name of the sheet to create
            public string xmlSheetName;  // SheetName=GoogleSheetName
            // names of extra columns to add to sheet
            public string [] addedColumns; // V1.1

            public string diagMode;
            public ConfigData()
            {
                diagMode = "*NONE*";

            }

        }

        /// <summary>
        /// Get a value from INI file, exit if not found
        /// </summary>
        /// <param name="ini"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        private string getConfigField(IniData ini, string fieldName)
        {
            string retval = ini.getWithSubstitution(fieldName);
            if (retval == null) ExitHandler.Instance.jfcExit(1, "Failed to find field " + fieldName + " in INI data file.");
            return retval;
        }


        /// <summary>
        /// Read configuration data from XML file
        /// </summary>
        private void readConfigData()
        {
            IniData ini = new IniData();
            string sts = ini.readParameterFile(configInfo.iniFileName);
            if (!sts.Equals("OK")) ExitHandler.Instance.jfcExit(1, "Error reading INI file: " + sts);

            configInfo.xmlTableName = getConfigField(ini, "TableName");
            configInfo.xmlRowName   = getConfigField(ini, "RowName");
            configInfo.gdriveFolder = getConfigField(ini, "GdriveFolder"); // name of tag that contains gdrive folder where we should upload the sheet
            configInfo.xmlSheetName = getConfigField(ini, "SheetName"); // name of tag that contains sheet name to use when uploading
            configInfo.addedColumns = getConfigField(ini, "AddColumns").Split(',');

            string diag = ini.getWithSubstitution("Debug"); // optional Debug=true;
            if (diag != null) configInfo.diagMode = diag;
            configInfo.formulaColumn = -1; // no column
            string tmp = ini.getWithSubstitution("Formula");
            if (tmp != null)
            {
                // optional Formula=F=A#*B#
                char col = tmp.ToUpper()[0]; // F
                if (Char.IsLetter(col))
                {
                    configInfo.formulaColumn = (int)col - (int)'A';
                    configInfo.formulaTemplate = tmp.Substring(1); // F=A#*B# -> "=A#*B#"
                }
            }

            // Google Sheets API calls this changing the row color,
            //    updateCells, rows, values, userEnteredFormat, backgroundColor
            configInfo.highlightSearchString = getConfigField(ini, "HighlightRowMarker"); // search string for highlighting a row
        }

        internal class SheetData
        {
            internal List<string> columnNames;
            internal List<List<string>> rows;
            internal SheetData()
            {
                columnNames = new List<string>();
                rows = new List<List<string>>();
            }

            // return as list of string arrays for adding to sheet
            public List<string[]> GetCsvUpdateData()
            {
                List<string[]> retval = new List<string[]>();
                // first, the column names
                string[] cols = columnNames.ToArray();
                int maxlen = cols.Length;
                retval.Add(cols);
                bool ragged = false;

                foreach (List<string> row in rows)
                {
                    string[] r = row.ToArray();
                    int len = r.Length;
                    if (len > maxlen)
                    {
                        maxlen = len;
                        ragged = true;
                    }
                    retval.Add(r);
                }
                // TODO - is it ok to be ragged?  Yes, seems to be
                return retval;
            }
        }

        ConfigData configInfo; // config info from the input INI file

        int nProcessed;

        /// <summary>
        /// Extract from input xml data the rows and columns that need to be uploaded to the sheet
        /// </summary>
        /// <param name="inputData"></param>
        /// <returns></returns>
        private SheetData extractDataToUpload(DataTree inputData)
        {
            // find the rows tags
            DataItem[] rows = inputData.FindAllTags(configInfo.xmlRowName);
            if ((rows == null) || (rows.Count()<1))
                ExitHandler.Instance.jfcExit(1, "No rows of data were found");

            bool firstRow = true;
            SheetData dataToUpload = new SheetData();
            foreach (DataItem row in rows)
            {
                List<string> rowdata = new List<string>();
                foreach (DataItem col in row.Subnodes)
                {
                    string val = col.Val;
                    rowdata.Add(val);
                    if (firstRow)
                    {
                        string colLabel = col.Tag;
                        dataToUpload.columnNames.Add(colLabel);
                    }
                }
                dataToUpload.rows.Add(rowdata);
                if (firstRow)
                {
                    if (configInfo.addedColumns != null)
                    {
                        // note - gdrive doesn't care if the columns are longer than the data (ragged)
                        foreach (string s in configInfo.addedColumns)
                            dataToUpload.columnNames.Add(s); // V1.2
                        // V1.3 - add requested formula
                        if (configInfo.formulaColumn >= 0)
                        {
                            while (dataToUpload.columnNames.Count() < configInfo.formulaColumn) dataToUpload.columnNames.Add("");
                            //dataToUpload.columnNames[configInfo.formulaColumn] = ""; --- column name in configInfo.addedColumns
                        }

                        // V1.3 - add sumation formula
                        char colId = (char)((int)'A' + configInfo.formulaColumn);
                        string grandTotalFormula = "=sum(" + colId + "2:" + colId + rows.Length.ToString() + ")";
                        dataToUpload.columnNames.Add(grandTotalFormula);
                    }
                }
                firstRow = false;
            }
            return dataToUpload;
        }

        /// <summary>
        /// Convert formula template to formula with row numbers
        /// </summary>
        /// <param name="template"></param>
        /// <param name="rowno"></param>
        /// <returns></returns>
        private string ConvertFormula(string template, int rowno)
        {
            // "=A#*C#", 2 becomes "=A2*C2"
            string retval = template.Replace("#",rowno.ToString());
            return retval;
        }

        /// <summary>
        /// Apply the business logic
        /// </summary>
        private void applyBusinessLogic()
        {
            nProcessed = 0; // change report of this below...!!!

            // Business logic
            // read input xml and extract rows/columns of the data to prepare for upload
            DataTree inputData = new DataTree("document");
            string sts = inputData.readXml(configInfo.inputFileName);
            if (sts != "OK")
                ExitHandler.Instance.jfcExit(1, "Error reading input XML file: " + sts);

            SheetData dataToUpload = extractDataToUpload(inputData);
            // TODO - right now we don't use outter tag for rows
            // TODO - nested folders
            //

            // extract sheet name from input xml
            DataItem sheetNameItem = inputData.FindMatchingNode(configInfo.xmlSheetName);
            if (sheetNameItem == null)
                ExitHandler.Instance.jfcExit(1, "Google sheet name field not found in input XML: " + configInfo.xmlSheetName);


            // create required folder
            // If folder already exists - keep it, if not, create it
            PushToGoogleSheets sheets = new PushToGoogleSheets();
            List<string> files = sheets.ListFiles("");

            string folderId = sheets.GetFolderId(configInfo.gdriveFolder);

            // only create folder if it doesn't exist
            if (folderId == null)
            {
                if (configInfo.diagMode != null) 
                    Console.WriteLine("Creating folder: " + configInfo.gdriveFolder);
                folderId = sheets.CreateFolder(configInfo.gdriveFolder);
                if (folderId == null)
                    ExitHandler.Instance.jfcExit(1, "Unable to create folder: " + configInfo.gdriveFolder);
            }
            else
            {
                if (configInfo.diagMode != null) 
                    Console.WriteLine("Using existing folder: " + configInfo.gdriveFolder);
            }

            // if already exists, exit with error
            if (files.Contains(configInfo.gdriveFolder+"/"+sheetNameItem.Val))
                ExitHandler.Instance.jfcExit(1, "Sheet " + sheetNameItem.Val + " already exists in folder "+configInfo.gdriveFolder);

            // create sheet

            if (configInfo.diagMode != null) Console.WriteLine("Creating sheet: " + sheetNameItem.Val);
            sts = sheets.CreateSheet(sheetNameItem.Val);
            if (sts != "OK")
                ExitHandler.Instance.jfcExit(1, "Unable to create folder: " + configInfo.gdriveFolder + ",  " + sts);

            // upload data to sheet
            List<string[]> csvData = dataToUpload.GetCsvUpdateData();
            // what if data is ragged? Not a problem for gdrive uploading if more column names than columns in data
            if (configInfo.diagMode != null) Console.WriteLine("Uploading "+csvData.Count()+" rows");
            
            // V1.3 - add formula if requested
            if (configInfo.formulaColumn >= 0)
            {
                // append formula to every row
                
                for (int rowIdx = 0; rowIdx < csvData.Count(); rowIdx++)
                {
                    if (rowIdx > 0)
                    {
                        int rowNum = rowIdx + 1;
                        List<string> row = new List<string>(csvData[rowIdx]);
                        while (row.Count() <= configInfo.formulaColumn) row.Add("");
                        row[configInfo.formulaColumn] = ConvertFormula(configInfo.formulaTemplate, rowNum);
                        rowNum++;
                        csvData[rowIdx] = row.ToArray();
                    }
                }
            }

            
            sts = sheets.AppendRows(csvData, configInfo.highlightSearchString);
            if (sts != "OK")
                ExitHandler.Instance.jfcExit(1, "Error uploading data: " + sts);
            nProcessed = csvData.Count();

            sts = sheets.FreezeFirstRow(); // V1.3
            if (sts != "OK")
                ExitHandler.Instance.jfcExit(1, "Error freezing first row: " + sts);

            // now that the sheet is created and populated, we need to move to the required folder.
            sts = sheets.MoveSheetToFolder(sheets.GetId(), folderId);
            if (sts != "OK")
                ExitHandler.Instance.jfcExit(1, "Error moving sheet to folder: " + sts);

            // TODO - req 16 - if any rows do not contain an existing column...

            // add two columns to input XML (req 21)
            DataItem sheetId = new DataItem("GoogleSheetId", sheets.GetId(), false);
            DataItem sheetUrl = new DataItem("GoogleSheetURL", sheets.GetUrl(), false);
            inputData.RootNode.Add(sheetId);
            inputData.RootNode.Add(sheetUrl);

            // write output XML
            try
            {
                if (configInfo.diagMode != null) Console.WriteLine("Writing output data file: "+configInfo.outputFileName);
                StreamWriter fout = new StreamWriter(configInfo.outputFileName);
                XmlWriter.writeXmlHeader(fout);
                XmlWriter.writeDataTree(inputData, fout);
                fout.Close();
            }
            catch (Exception e)
            {
                ExitHandler.Instance.jfcExit(1, "Error writing output file: " + e.Message);
            }

        }



        /// <summary>
        /// run method - top level logic for the application resides here
        /// </summary>
        /// <param name="args">command line args</param>
        private void run(string[] args)
        {
            SignOn();

            if ((args.Length == 1) && (args[0]=="authorize"))
            {
                // one time
                Console.WriteLine("Authorization in progress");
                try
                {
                    Authorization.RequestAuthorization();
                    Console.WriteLine("Authorization successful. You may now use this program with no user interaction.");
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Authorization failed. Message: " + ex.Message);
                }
                System.Environment.Exit(1);
            }
            else if (args.Length < 4)
            {
                Console.Out.WriteLine("Usage: " + sProgName + " input.xml  output.dat  input.ini  log.file");
                System.Environment.Exit(1); // error exit code
            }

            // grab file names from command line
            configInfo = new ConfigData();
            configInfo.inputFileName  = args[0];
            configInfo.outputFileName = args[1];
            configInfo.iniFileName    = args[2];
            configInfo.jfcLogFileName = args[3];

            
            // initialize the exit and status handler
            ExitHandler.init(sProgName, configInfo.jfcLogFileName);

            // license check if needed
            //int privs = ExitHandler.Instance.getEntry("PDFFIL");
            //if (privs == 0)
            //{
            //    ExitHandler.Instance.jfcExit(1, "Unable to locate product license");
            //}

            // read INI file
            readConfigData();

            // apply business logic from SPEC
            applyBusinessLogic();

            // and we're all done, this was a normal run without detected fatal errors
            ExitHandler.Instance.jfcExit(0, "Processing completed, " + nProcessed + " rows.");
        }



        /// <summary>
        /// Main operating system entry point, OS parses command line arguments
        /// and then activates this function with an array of string arguments.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Program program = new Program();

            program.run(args);

            System.Environment.Exit(0); // normal exit, status code 0
        }

    }
}
